<?php

//Include the stream parser class
include "parser.php";

new garbage($argv);

class garbage{
    
    /**
     * Start the process
     * @param array $arg Command line arguments
     */
    public function __construct() {
        global $argv;
        $this->run($argv); 
    }

    /**
     * Check and load the file, start up the parser.
     * @param array $arg Command line arguments
     */    
    private function run($arg){
        
        $this->log("Garbage analysis");
        
        //Check there is a filename
        if(!is_array($arg) || !isset($arg[1])){
            $this->log("Usage: php garbage.php filename\nNo filename provided");
            die();
        }
        
        //Check file exists
        $filename=$arg[1];
        if(!file_exists($filename)){
            $this->log("Could not locate file named {$filename}. Please check the filename and folder are correct.");
            die();
        }
        
        //Check if verbose flag set
        $verbose=false;
        if(isset($arg[2]) && strtolower($arg[2])=='verbose'){
            $verbose=true;
        }
        
        //Load the file contents.
        $stream=file_get_contents($filename);
        
        //Instatiate and run the parse
        $parser= new parser($stream);
        $result=$parser->parseStream($verbose);

        //Report the result
        $this->log("Score for stream is {$result}");        
    }
    
    /**
     * Log output message
     * @param string|array|object $msg
     */
    private function log($msg){
        if(is_array($msg) || is_object($msg)){
            print_r($msg);
        } else {
            echo $msg . "\n";
        }
    }
}