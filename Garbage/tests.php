<?php

/* 
 * Tests for the garbage stream parser;
 * 
 */
echo '<pre>';
include "parser.php";

new garbageTests();

class garbageTests{

    /**
     *
     * @var array Test data
     */
    private $data;
    
    public function __construct(){
        global $argv;
        $this->test($argv);
    }
    
    public function test($arg){
        $this->log("Garbage tests");
        
        //Check if verbose flag set
        $verbose=false;
        if(is_array($arg) &&  isset($arg[1]) && strtolower($arg[1])=='verbose'){
            $verbose=true;
        }

        $this->loadData();
        //Instatiate the parser
        $parser=new parser();
        
        //Results
        $totalTests=0;
        $totalFailed=0;
        
        foreach($this->data as $stream=>$expected){
            $totalTests++;
            
            $parser->init($stream);
            $result=$parser->parseStream($verbose);
            
            $msg="{$totalTests}: {$stream} - Expected {$expected} Actual {$result} - Result:";
            if($result!==$expected){
                $totalFailed++;
                $msg.="FAIL\n\n";
            } else {
                $msg.="pass\n\n";
            }
            $this->log($msg);
        }

        $this->log("\nTests completed. Number of tests - {$totalTests}. Number failed - {$totalFailed}");
    }
    
    private function loadData(){
        $this->data=[
            //Score tests
            '{}'=>1,
            '{{{}}}'=>6,
            '{{},{}}'=>5,
            '{{{},{},{{}}}}'=>16,
            '{<a>,<a>,<a>,<a>}'=>1,
            '{{<ab>},{<ab>},{<ab>},{<ab>}}'=>9,
            '{{<!!>},{<!!>},{<!!>},{<!!>}}'=>9,
            '{{<a!>},{<a!>},{<a!>},{<ab>}}'=>3,
            
            //Invalid stream tests
            'a{}'=>false,
            '<a>a'=>false,
            '{}}'=>false,
            '{{}}!}}'=>false
        ];
    }
    
    /**
     * Log output message
     * @param string|array|object $msg
     */
    private function log($msg){
        if(is_array($msg) || is_object($msg)){
            print_r($msg);
        } else {
            echo $msg . "\n";
        }
    }    
}