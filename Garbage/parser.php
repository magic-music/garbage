<?php

/**
 * Garbage stream parser
 * 
 * Adam Hembrough
 * 2018-04-04
 */


/**
 * Parse a stream of data passed during instantiation
 * Report the stream score or false if invalid stream
 */
class parser{
    /**
     *
     * @var string The entire stream of characters; 
     */
    private $stream;
    
    /**
     *
     * @var int The number of characters in the stream;
     */
    private $streamLength;
    
    /**
     *
     * @var int The current position in the stream;
     */
    private $cursor;
    
    /**
     *
     * @var int Running total for the stream 
     */
    private $score;
    
    /**
     *
     * @var int The score value for the current group
     */
    private $groupScore;
    
    /**
     * 
     * @param bool True when garbage opening bracket found;
     */
    private $garbageFlag;
    
    /**
     *
     * @var bool For testing - when true report progress;
     */
    private $verbose;
    
    /**
     * Initialise the parser with stream data if provided during instantiation
     * @param string [optional] $stream the character stream to test
     */
    public function __construct($stream=false){
        if($stream){
            $this->init($stream);
        }
    }
    
    /**
     * Initialise stream and parser properties
     * @param string $stream The character stream to parse
     */
    public function init($stream){
        //Store the character stream
        $this->stream=$stream;
        
        //Cache the stream length to save recalulating each time
        $this->streamLength=strlen($stream);
        
        //Initialise the cursor position
        $this->cursor=-1;        
        
        //Scores and flags to initial values
        $this->garbageFlag=false;
        $this->groupScore=0;
        $this->score=0;      
    }
    
    /**
     * Parse the current stream, return the score;
     * @param bool $verbose When true, log progress
     * @return int|bool The total stream score or false if stream is invalid     
     */
    public function parseStream($verbose=false){
        //Logging flag
        $this->verbose=$verbose;
        
        if(!$this->stream){
            $this->log("No character stream set");
            $valid=false;
        } else {
            //Loop through the stream
            $valid=$this->loopStream();
        }

        if(!$valid){
            $this->log("Invalid stream. Parsing halted");
            //Falsify the total
            $this->score=false;
        } else {
            $this->log("End of stream reached. Final score is " . $this->score);
        }    
        
        //Report score or false
        return $this->score;
    }
    
    /**
     * Loop through the character stream
     * @return bool true if the stream was valid
     */
    private function loopStream(){
        //Assume valid stream to start
        $valid=true;
        
        do{
            //Parse each character
            $char=$this->nextChar();
            if($char){
                $this->log("Character $char");
                $valid=$this->parseChar($char);
            }        
        } while($char && $valid);    
        
        return $valid;
    }
    
    /**
     * Act on the given character, report result
     * @param string $char The character to parse
     * @return boolean false if invalid character found
     */
    public function parseChar($char){
        //Garbage character flag
        $garbage=false;
        
        //Valid character flag
        $valid=true;
        
        switch($char){
            case '!':
                $this->log("Ignore next character");
                $this->cursor++;
                break;

            case '<':
                $this->log("Beginning of garbage");
                $this->garbageFlag=true;
                break;

            case '>':
                $this->log("End of garbage");
                $this->garbageFlag=false;
                break;

            case '{':
                if(!$this->garbageFlag){
                    $this->log("Beginning of new group - increment group score");
                    $this->groupScore++;
                }
                break;

            case '}';
                if(!$this->garbageFlag){
                    $this->log("End of group - add group score to total and decrement score for next group");
                    $this->score += $this->groupScore;
                    $this->groupScore--;
                }
                break;
            
            case ',':
                if($this->garbageFlag){
                    $this->log("Garbage");
                    $garbage=true;
                } else {
                    $this->log("Group separator");
                }
                break;
                
            default:
                $this->log("Garbage character");
                $garbage=true;
        }        
        
        //Check for invalid stream data;
        if($garbage && !$this->garbageFlag){
            $this->log("Garbage found outside garbage brackets");
            $valid=false;
        }
        
        if($this->groupScore<0){
            $this->log("Invalid group closing bracket found");
            $valid=false;
        }
        
        return $valid;
    }
    
    /**
     * Advance the cursor, get the next character
     * @return string|bool The next character or false if reached the end of the stream
     */
    private function nextChar(){
        //Assume no charactet to start
        $char=false;
        
        //Advance the cursor
        $this->cursor++;
        if($this->cursor < $this->streamLength){
            //Get the character
            $char=substr($this->stream,$this->cursor,1);
        }
        return $char;
    }
    
    /**
     * Log output message
     * @param string|array|object $msg
     */
    private function log($msg){
        if($this->verbose){
            if(is_array($msg) || is_object($msg)){
                print_r($msg);
            } else {
                echo $msg . "\n";
            }
        }
    }
}
