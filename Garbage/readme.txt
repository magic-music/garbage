This code is a solution to the problem posed at https://adventofcode.com/2017/day/9

It runs from the command line.

To run tests:
php tests.php

For verbose output:
php tests.php verbose

To run the code against the given input stream:
php garbage.php stream.txt

To run with verbose output:
php garbage.php stream.txt verbose


Final correct score for the given input stream is 16869